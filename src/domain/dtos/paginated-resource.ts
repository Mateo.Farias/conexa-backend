import { Resource } from "../entities/resource";
import { Paginated } from "./paginated";

export type TPaginatedResourceResponse = Paginated<Resource>;
