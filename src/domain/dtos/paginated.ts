export interface Paginated<T> {
  currentPage: number;
  pages: number;
  total: number;
  items: T[];
}
