export interface IPerson {
  name: string;
  birth_year: string;
  eye_color: string;
  gender: TGender;
  hair_color: string;
  height: string;
  mass: string;
  skin_color: string;
  homeworld: string;
  films: string[];
  species: string[];
  starships: string[];
  vehicles: string[];
  created: string;
  edited: string;
  url: string;
}

export type TGender = "Male" | "Female" | "unknown" | "n/a";
