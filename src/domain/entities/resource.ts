export type TResourceName = "films" | "planets" | "people" | "starships";

export interface Resource {
  id: string;
  name: string;
  created: string;
  edited: string;
}
