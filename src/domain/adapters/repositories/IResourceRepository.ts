import { Paginated } from "src/domain/dtos/paginated";
import { IFilm } from "src/domain/entities/films";
import { IPerson } from "src/domain/entities/people";
import { IPlanet } from "src/domain/entities/planet";
import { TResourceName } from "src/domain/entities/resource";
import { IStarship } from "src/domain/entities/starships";

export interface IResourceRepository {
  getAll<T extends TResourceName>(
    resource: T,
    page: number,
    query?: string
  ): Promise<TResourceGetAllResult<T>>;
  getOne<T extends TResourceName>(
    resource: T,
    id: string
  ): Promise<TResourceGetOneResult<T>>;
}

export type TPaginatedResourceResult<T> = Paginated<T>;

export type TResourceGetAllResult<T> = T extends "films"
  ? TPaginatedResourceResult<IFilm>
  : T extends "people"
    ? TPaginatedResourceResult<IPerson>
    : T extends "starships"
      ? TPaginatedResourceResult<IStarship>
      : T extends "planets"
        ? TPaginatedResourceResult<IPlanet>
        : never;

export type TResourceGetOneResult<T> = T extends "films"
  ? Promise<IFilm>
  : T extends "people"
    ? Promise<IPerson>
    : T extends "starships"
      ? Promise<IStarship>
      : T extends "planets"
        ? Promise<IPlanet>
        : never;
