import { NestFactory } from "@nestjs/core";
import { AppModule } from "./infrastructure/modules/app.module";
import { ConfigService } from "@nestjs/config";
import { Logger, VersioningType } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger("Bootstrap");
  const configService = app.get(ConfigService);
  const port = configService.get("port");
  app.setGlobalPrefix("/api");
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: "1",
  });
  app.enableCors();

  await app.listen(port);
  logger.log(`Starwars Conexa app listening  on: ${port}`);
}

bootstrap();
