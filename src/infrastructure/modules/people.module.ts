import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { PeopleController } from "../controllers/people.controller";
import { ResourceService } from "../services/resource.service";
import { ResourceModule, ResourceServiceFactory } from "./resource.module";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";

@Module({
  imports: [ConfigModule, HttpModule, ResourceModule],
  controllers: [PeopleController],
  providers: [ResourceService, ResourceServiceFactory, SWAPIResourceRepository],
})
export class PeopleModule {}
