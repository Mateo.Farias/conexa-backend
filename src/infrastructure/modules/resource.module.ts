import { HttpModule } from "@nestjs/axios";
import { Module, Provider } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { ResourceService } from "../services/resource.service";
import { IResourceRepository } from "src/domain/adapters/repositories/IResourceRepository";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";

export const ResourceServiceFactory: Provider = {
  provide: ResourceService,
  useFactory: (resourceRepository: IResourceRepository) => {
    return new ResourceService(resourceRepository);
  },
  inject: [SWAPIResourceRepository],
};

@Module({
  imports: [ConfigModule, HttpModule],
  providers: [ResourceServiceFactory, SWAPIResourceRepository],
  exports: [ResourceServiceFactory, ResourceService],
})
export class ResourceModule {}
