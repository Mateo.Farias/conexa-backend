import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { AppController } from "../controllers/app.controller";
import { AppService } from "../services/app.service";
import appConfig from "../configuration/appConfig";
import { PeopleModule } from "./people.module";
import { FilmsModule } from "./films.module";
import { StarshipsModule } from "./starships.module";
import { ResourceModule } from "./resource.module";
import { PlanetsModule } from "./planets.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
      isGlobal: true,
    }),
    PeopleModule,
    FilmsModule,
    StarshipsModule,
    ResourceModule,
    PlanetsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
