import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { PlanetsController } from "../controllers/planets.controller";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";
import { ResourceService } from "../services/resource.service";
import { ResourceModule, ResourceServiceFactory } from "./resource.module";

@Module({
  imports: [ConfigModule, HttpModule, ResourceModule],
  controllers: [PlanetsController],
  providers: [ResourceService, ResourceServiceFactory, SWAPIResourceRepository],
})
export class PlanetsModule {}
