import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { StarshipsController } from "../controllers/starships.controller";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";
import { ResourceService } from "../services/resource.service";
import { ResourceModule, ResourceServiceFactory } from "./resource.module";

@Module({
  imports: [ConfigModule, HttpModule, ResourceModule],
  controllers: [StarshipsController],
  providers: [ResourceService, ResourceServiceFactory, SWAPIResourceRepository],
})
export class StarshipsModule {}
