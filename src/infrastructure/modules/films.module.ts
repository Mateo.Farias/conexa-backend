import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { FilmsController } from "../controllers/films.controller";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";
import { ResourceService } from "../services/resource.service";
import { ResourceModule, ResourceServiceFactory } from "./resource.module";

@Module({
  imports: [ConfigModule, HttpModule, ResourceModule],
  controllers: [FilmsController],
  providers: [ResourceService, ResourceServiceFactory, SWAPIResourceRepository],
})
export class FilmsModule {}
