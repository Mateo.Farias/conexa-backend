import { BadRequestException } from "@nestjs/common";
import {
  IResourceRepository,
  TPaginatedResourceResult,
} from "src/domain/adapters/repositories/IResourceRepository";
import { TPaginatedResourceResponse } from "src/domain/dtos/paginated-resource";
import { IFilm } from "src/domain/entities/films";
import { IPerson } from "src/domain/entities/people";
import { IPlanet } from "src/domain/entities/planet";
import { Resource, TResourceName } from "src/domain/entities/resource";
import { IStarship } from "src/domain/entities/starships";

export class ResourceService {
  constructor(private readonly resourceRepository: IResourceRepository) {}

  async getResourceList(resource: TResourceName, page: number, query?: string) {
    const resourceList = await this.resourceRepository.getAll(
      resource,
      page,
      query
    );
    return this.resourceMapper(resource, resourceList);
  }

  async getResource(resource: TResourceName, id: string) {
    return await this.resourceRepository.getOne(resource, id);
  }

  private resourceMapper(
    resource: TResourceName,
    {
      currentPage,
      items,
      pages,
      total,
    }:
      | TPaginatedResourceResult<IFilm>
      | TPaginatedResourceResult<IPlanet>
      | TPaginatedResourceResult<IPerson>
      | TPaginatedResourceResult<IStarship>
  ): TPaginatedResourceResponse {
    const resourceList = items.map((item) => {
      switch (resource) {
        case "people":
          return this.peopleToResourceMapper(item);
        case "films":
          return this.filmsToResourceMapper(item);
        case "planets":
          return this.planetsToResourceMapper(item);
        case "starships":
          return this.starshipsToResourceMapper(item);
        default:
          throw new BadRequestException();
      }
    });

    return {
      currentPage,
      pages,
      total,
      items: resourceList,
    };
  }

  private peopleToResourceMapper(item: IPerson): Resource {
    return {
      id: this.SWAPIIdExtractor(item.url),
      name: item.name,
      created: item.created,
      edited: item.edited,
    };
  }

  private filmsToResourceMapper(item: IFilm): Resource {
    return {
      id: this.SWAPIIdExtractor(item.url),
      name: item.title,
      created: item.created,
      edited: item.edited,
    };
  }

  private starshipsToResourceMapper(item: IStarship): Resource {
    return {
      id: this.SWAPIIdExtractor(item.url),
      name: item.name,
      created: item.created,
      edited: item.edited,
    };
  }

  private planetsToResourceMapper(item: IPlanet): Resource {
    return {
      id: this.SWAPIIdExtractor(item.url),
      name: item.name,
      created: item.created,
      edited: item.edited,
    };
  }

  private SWAPIIdExtractor(url: string) {
    const match = url.match(/\/(\d+)\/$/);
    if (match && match[1]) {
      return match[1];
    }
  }
}
