import { Test, TestingModule } from "@nestjs/testing";
import { HttpModule } from "@nestjs/axios";
import { ResourceService } from "./resource.service";
import { SWAPIResourceRepository } from "../drivers/repositories/resource.rest.repository";
import { AppModule } from "../modules/app.module";
import { IFilm } from "src/domain/entities/films";

describe("ResourceService", () => {
  let service: ResourceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResourceService, SWAPIResourceRepository],
      imports: [HttpModule, AppModule],
    }).compile();

    service = module.get<ResourceService>(ResourceService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("GetResourceList with films resource argument should return an array of films", async () => {
    jest.setTimeout(20000);
    const films = await service.getResourceList("films", 1);
    expect(films.currentPage).toBe(1);
    expect(Array.isArray(films.items)).toBeTruthy;
    expect(films.items.length).toBeGreaterThan(0);

    for (const film of films.items) {
      expect(typeof film.id).toBe("string");
      expect(typeof film.name).toBe("string");
      expect(typeof new Date(film.created).getMonth === "function").toBeTruthy;
      expect(typeof new Date(film.edited).getMonth === "function").toBeTruthy;
    }
  }, 20000);

  it("GetResource should return a film detail", async () => {
    const film = (await service.getResource("films", "1")) as IFilm;

    expect(film).toBeDefined();
    expect(film.director).toBeDefined();
  });
});
