export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  swapiEndpoint: process.env.SWAPI_ENDPOINT,
});
