import { HttpService } from "@nestjs/axios";
import { HttpException, Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { AxiosRequestConfig } from "axios";
import {
  IResourceRepository,
  TResourceGetAllResult,
  TResourceGetOneResult,
} from "src/domain/adapters/repositories/IResourceRepository";
import { TResourceName } from "src/domain/entities/resource";

@Injectable()
export class SWAPIResourceRepository implements IResourceRepository {
  private readonly logger = new Logger(SWAPIResourceRepository.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {}

  async getAll<T extends TResourceName>(
    resource: T,
    page: number,
    query?: string
  ): Promise<TResourceGetAllResult<T>> {
    this.logger.log(
      `Searching for ${resource} list page: ${page}. ${query ? "With query: " + query : ""}`
    );
    return await this.fetchResourceList(resource, page, query);
  }

  async getOne<T extends TResourceName>(
    resource: T,
    id: string
  ): Promise<TResourceGetOneResult<T>> {
    this.logger.log(`Searching for person: ${id}`);
    return await this.fetchResource(resource, id);
  }

  private async fetchResourceList<T extends TResourceName>(
    resource: T,
    page: number,
    query: string
  ): Promise<TResourceGetAllResult<T>> {
    const url = `${this.configService.get("swapiEndpoint")}/${resource}`;
    const options: AxiosRequestConfig = {
      url,
      method: "GET",
      params: {
        page,
        search: query,
      },
    };
    this.logger.log(`Getting ${resource} list...`);
    return this.httpService.axiosRef
      .request(options)
      .then(({ data }) => {
        this.logger.log(`Got ${resource} list...`);
        return {
          pages: Math.ceil(data.count / 10),
          currentPage: page,
          total: data.count,
          items: data.results,
        } as TResourceGetAllResult<T>;
      })
      .catch((error) => {
        this.logger.error(`Error while getting ${resource} list: ${error}`);
        throw new HttpException(error.response.data, error.response.status);
      });
  }

  private async fetchResource<T extends TResourceName>(
    resource: T,
    id: string
  ): Promise<TResourceGetOneResult<T>> {
    const url = `${this.configService.get("swapiEndpoint")}/${resource}/${id}`;
    const options: AxiosRequestConfig = {
      url,
      method: "GET",
    };
    this.logger.log(`Getting ${resource}...`);
    return this.httpService.axiosRef
      .request(options)
      .then((res) => {
        this.logger.log(`Got ${resource}...`);
        return res.data;
      })
      .catch((error) => {
        this.logger.error(`Error while getting ${resource}: ${error}`);
        throw new HttpException(error.response.data, error.response.status);
      });
  }
}
