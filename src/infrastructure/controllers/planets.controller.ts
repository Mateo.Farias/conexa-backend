import { Controller, Get, Param, Query, ValidationPipe } from "@nestjs/common";
import { PaginationDTO } from "../../utils/dtos/pagination.dto";
import { ResourceService } from "../services/resource.service";
import { TResourceName } from "../../domain/entities/resource";

@Controller("/planets")
export class PlanetsController {
  private readonly resourceName: TResourceName = "planets";
  constructor(private readonly resourceService: ResourceService) {}

  @Get()
  getPlanets(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
      })
    )
    pagination: PaginationDTO
  ) {
    return this.resourceService.getResourceList(
      this.resourceName,
      pagination.page,
      pagination.query
    );
  }

  @Get(":id")
  getPlanet(@Param("id") id: string) {
    return this.resourceService.getResource(this.resourceName, id);
  }
}
