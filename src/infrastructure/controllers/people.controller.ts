import { Controller, Get, Param, Query, ValidationPipe } from "@nestjs/common";
import { PaginationDTO } from "../../utils/dtos/pagination.dto";
import { ResourceService } from "../services/resource.service";
import { TResourceName } from "../../domain/entities/resource";

@Controller("/people")
export class PeopleController {
  private readonly resourceName: TResourceName = "people";
  constructor(private readonly resourceService: ResourceService) {}

  @Get()
  getPeople(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
      })
    )
    pagination: PaginationDTO
  ) {
    return this.resourceService.getResourceList(
      this.resourceName,
      pagination.page,
      pagination.query
    );
  }

  @Get(":id")
  getPerson(@Param("id") id: string) {
    return this.resourceService.getResource(this.resourceName, id);
  }
}
