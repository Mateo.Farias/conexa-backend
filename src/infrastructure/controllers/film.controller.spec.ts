import * as request from "supertest";
import { FilmsModule } from "../modules/films.module";
import { INestApplication } from "@nestjs/common";
import {
  filmResponseMock,
  paginatedResourceResponseMock,
  resourceItemMock,
} from "../../../test/mocks/resource-mock";
import { ResourceService } from "../services/resource.service";
import { Test } from "@nestjs/testing";

describe("Films Controller Tests", () => {
  let app: INestApplication;

  const resourceService = {
    getResourceList: () => Promise.resolve(paginatedResourceResponseMock),
    getResource: () => Promise.resolve(filmResponseMock),
    resourceMapper: () => paginatedResourceResponseMock,
    peopleToResourceMapper: () => resourceItemMock,
    filmsToResourceMapper: () => resourceItemMock,
    starshipsToResourceMapper: () => resourceItemMock,
    planetsToResourceMapper: () => resourceItemMock,
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [FilmsModule],
    })
      .overrideProvider(ResourceService)
      .useValue(resourceService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`Fail to get films missing page`, async () => {
    return request(app.getHttpServer())
      .get("/films")
      .expect(400)
      .expect({
        message: [
          "page must be a number conforming to the specified constraints",
        ],
        error: "Bad Request",
        statusCode: 400,
      });
  });

  it(`Get films`, async () => {
    return request(app.getHttpServer())
      .get("/films?page=1")
      .expect(200)
      .expect(paginatedResourceResponseMock);
  });

  it(`Get one film`, async () => {
    return request(app.getHttpServer())
      .get("/films/1")
      .expect(200)
      .expect(filmResponseMock);
  });

  afterAll(async () => {
    await app.close();
  });
});
