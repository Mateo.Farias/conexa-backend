import { Controller, Get, Param, Query, ValidationPipe } from "@nestjs/common";
import { TResourceName } from "../../domain/entities/resource";
import { PaginationDTO } from "../../utils/dtos/pagination.dto";
import { ResourceService } from "../services/resource.service";

@Controller("/starships")
export class StarshipsController {
  private readonly resourceName: TResourceName = "starships";
  constructor(private readonly resourceService: ResourceService) {}

  @Get()
  getStarships(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
      })
    )
    pagination: PaginationDTO
  ) {
    return this.resourceService.getResourceList(
      this.resourceName,
      pagination.page,
      pagination.query
    );
  }

  @Get(":id")
  getStarship(@Param("id") id: string) {
    return this.resourceService.getResource(this.resourceName, id);
  }
}
