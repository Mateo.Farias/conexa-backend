import { Controller, Get, Param, Query, ValidationPipe } from "@nestjs/common";
import { PaginationDTO } from "../../utils/dtos/pagination.dto";
import { ResourceService } from "../services/resource.service";
import { TResourceName } from "../../domain/entities/resource";

@Controller("/films")
export class FilmsController {
  private readonly resourceName: TResourceName = "films";
  constructor(private readonly resourceService: ResourceService) {}

  @Get()
  getFilms(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
      })
    )
    pagination: PaginationDTO
  ) {
    return this.resourceService.getResourceList(
      this.resourceName,
      pagination.page,
      pagination.query
    );
  }

  @Get(":id")
  getFilm(@Param("id") id: string) {
    return this.resourceService.getResource(this.resourceName, id);
  }
}
