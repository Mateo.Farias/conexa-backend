import { IsNumber } from "class-validator";
import { SearchableDTO } from "./searchable.dto";

export class PaginationDTO extends SearchableDTO {
  @IsNumber()
  page: number;
}
