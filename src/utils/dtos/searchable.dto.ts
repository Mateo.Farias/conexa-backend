import { IsOptional, IsString } from "class-validator";

export class SearchableDTO {
  @IsOptional()
  @IsString()
  query: string;
}
