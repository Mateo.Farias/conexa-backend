# Conexa Full-Stack Challenge - Backend

BFF NestJS project - Backend that consumes public Star Wars API data for a web app.

Uses Clean architecture structure, for maintainability and scalability.

## Installation

** Requires node v16 or higher **

Fill .env template

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run dev

# production mode
$ npm run prod
```
